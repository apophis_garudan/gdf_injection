# README #
GDF-Injection = Google-Drive-Folders Injection
### GDF-Injection is an collection of javascript functions to inject google-drive folders into webpages

### What is this repository for? ###

* to inject google drive folders into webpages
* to show the folders as folder-tree
* to collapse the folders
* to show files in google-viewer (if supported)

### How do I get set up? ###

* clone or download this repository and add the gdf-injection.js in the <head> tag of your main html-page
* in gdf-injection.js set:
	* CLIENT_ID with your client id
	* ROOT_FOLDER_TITLE with your root folder title from your google drive
* !!! IMPORTANT !!!
	* Required libraris are:
		* https://apis.google.com/js/client.js?onload=checkAuth
		* https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js
* in your html-page declare:
	* a tag with id="folder-tree", this will be replaced with your subfolders from google-drive after if authorization was successfull
	* a tag with id="file-view", this will be replaced with google-viewer and display the selected file

### Sample ###
* Without styles it will be look like:
![Alt text](/design/gdf_injection_screenshot.png)
### Who do I talk to? ###

* Nirosan Bala : Developer