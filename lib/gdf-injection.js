

// ########################################################### Google Drive specified functions ##########################################################

/**
 * Drive client id which is registered in your console.developers.google account
 */
const CLIENT_ID = "<your-client-id>";
/**
 * Scope of drive contents (example: readonly)
 */
const SCOPE = "https://www.googleapis.com/auth/drive.readonly";
/**
 * Title of root folder in google drive  (example: Dokumentation)
 */
var ROOT_FOLDER_TITLE = "<your-root-folder-title>";
/**
 * Mimetype of root folder
 */
var ROOT_FOLDER_MIMETYPE = "application/vnd.google-apps.folder";
/**
 * To store files
 */
var files = new Array();
/**
 * To store folders
 */
var folders = new Array();
/**
 * To store request of gapi
 */
var request;


/**
 * Initial function to authorizise into google drive
 * @param {event} event - Clickevent
 */
gapiAuthorization = function (event) {
    // call gapi authorize function with authorize data object and callback function
    gapi.auth.authorize(
        {
            client_id: CLIENT_ID,
            scope: SCOPE,
            immediate: false
        },
        handleAuthResponse);
    // If gapi authorize function is not available
    return false;
}


/**
 * Handle gapi authorization response
 * @param {object} response - gapi authorization response
 */
handleAuthResponse = function (response) {

    if (response && !response.error) {
        // If gapi authorization response is success
        // Load gapi drive
        loadDriveApi();
    } else {
        // gapi authorization is failed
    }
}

/**
 * Load gapi drive 
 */
loadDriveApi = function () {
    gapi.client.load('drive', 'v2', function () {
        // if drive loaded find root folder with callback funciton to load subfolders and files
        findRootFolder(loadSubFoldersAndMapToFiles);
    });
}

/**
 * Find root folder
 * @param {function} loadSubFoldersAndMapToFiles - callback function to load subfolders and files
 */
findRootFolder = function (loadSubFoldersAndMapToFiles) {

    var retrievePageOfFiles = function (request, result) {
        request.execute(function (response) {
            result = result.concat(response.items);
            var nextPageToken = response.nextPageToken;
            if (nextPageToken) {
                request = gapi.client.drive.files.list(
                    {
                        'pageToken': nextPageToken
                    }
                );

                retrievePageOfFiles(request, result);
            } else {
                var ROOT_FOLDER_ID;
                for (var i = 0; i < result.length; ++i) {
                    files[i] = result[i];
                    if(files[i].title == ROOT_FOLDER_TITLE 
                        && files[i].mimeType == ROOT_FOLDER_MIMETYPE){
                           // console.log(files[i])
                           ROOT_FOLDER_ID = files[i].id;
                           break;
                        }
                }
                if(ROOT_FOLDER_ID != null && ROOT_FOLDER_ID != undefined && ROOT_FOLDER_ID != ''){
                    loadSubFoldersAndMapToFiles(ROOT_FOLDER_ID, files, createFolderTree);
                }
                
            }
        });
    }

    var initialRequest = gapi.client.drive.files.list();
    retrievePageOfFiles(initialRequest, []);
}

/**
 * load all subfolders of given folder and map/store files to specified subfolders
 * @param {string} folderID - parent folder id
 * @param {array} files - all files of gapi drive
 * @param {function} createFolderTree - callback function to create folder-tree 
 */
loadSubFoldersAndMapToFiles = function (folderID, files, createFolderTree) {

    var retrievePageOfChildren = function (request, result) {
        request.execute(function (response) {
            result = result.concat(response.items);
            var nextPageToken = response.nextPageToken;
            if (nextPageToken) {
                request = gapi.client.drive.children.list(
                    {
                        'folderId': folderID,
                        'pageToken': nextPageToken
                    }
                );
                retrievePageOfChildren(request, result);
            } else {
                for (var i = 0; i < result.length; ++i) {
                    folders[i] = result[i];
                }
                createFolderTree(folders, files);
            }
        });

    }

    var initialRequest = gapi.client.drive.children.list(
        {
            'folderId': folderID
        }
    );

    retrievePageOfChildren(initialRequest, []);
}


// ############################################################################## END of Google Drive specified functions ##########################################################


// ########################################################### Functions to create folder-tree ##########################################################


/**
 * Displays folders as tree
 * @param {array} folders - all folders
 * @param {array} files - all files
 */
createFolderTree = function (folders, files) {
    var topTreeFolder = $('<ul id="folder-tree">');

    for (var i = 0; i < folders.length; ++i) {
        var folder_id = folders[i].id;
        for (var j = 0; j < files.length; ++j) {
            // TODO Check this file_id 
            var file_id = files[j].id;
            if (folder_id == file_id) {
                var title = files[j].title;
                var subTreeFolder = $('<li id="' + folder_id + '"><a onclick="collapseFolder(\'' + folder_id + '\')">' + title + '</a></li>');
                topTreeFolder.append(subTreeFolder);
            }
        }
    }

    $('#folder-tree').replaceWith(topTreeFolder);
}

/**
 * Collapse selected folder
 * @param {string} folderId - Id of selected folder
 */
collapseFolder = function (folderId) {

    if ($('#sub-tree' + folderId).children().length > 0) {
        $("#sub-tree" + folderId).remove();
    } else {
        var retrievePageOfChildren = function (request, result) {
            request.execute(function (response) {
                result = result.concat(response.items);

                var topTreeFolder = $('<ul id="sub-tree' + folderId + '">');
                for (var i = 0; i < result.length; ++i) {
                    var folder_id = result[i].id;
                    for (var j = 0; j < files.length; ++j) {
                        file_id = files[j].id;
                        if (folder_id == file_id) {
                            var title = files[j].title;
                            var mimeType = files[j].mimeType;
                            var subTreeFolder;
                            if (mimeType.endsWith('folder')) {
                                subTreeFolder = $('<li id="' + folder_id + '"><a onclick="collapseFolder(\'' + folder_id + '\')">' + title + '</a></li>');

                            } else {
                                subTreeFolder = $('<li id="' + folder_id + '"><a onclick="showFile(\'' + folder_id + '\')">' + title + '</a></li>');
                            }

                            topTreeFolder.append(subTreeFolder);
                        }
                    }
                }

                $('#' + folderId).append(topTreeFolder);

            });
        }

        var initialRequest = gapi.client.drive.children.list({
            'folderId': folderId
        });

        retrievePageOfChildren(initialRequest, []);

    }
}

/**
 * Shows selected file via google-viewer
 * @param {string} fileId - Id of selected file
 */
showFile = function (fileId) {
    var docFile = $('<div style="margin-left:300px;" id="file-view">');
    var iframeDocFile = $('<iframe src="https://docs.google.com/viewer?srcid=' + fileId + '&pid=explorer&efh=false&a=v&chrome=false&embedded=true"'
        + 'width="100%" height="600px">'
        + '</iframe>');
    docFile.append(iframeDocFile);
    $('#file-view').replaceWith(docFile);
}

// ############################################################################## END of Functions to create folder-tree ##########################################################
